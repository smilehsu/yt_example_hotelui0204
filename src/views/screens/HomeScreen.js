import React, { useState, useRef } from 'react';
import {
    Dimensions,
    FlatList,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    Image,
    Animated, e
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Component } from 'react/cjs/react.development';
import COLORS from '../../consts/colors';
import hotels from '../../consts/hotels';

const { width } = Dimensions.get("screen");
const CardWidth = width / 1.8

const HomeScreen = ({ navigation }) => {
    const categories = ['All', 'Popular', 'Top Rated', 'Featured', 'Luxury'];
    const [selectedCategoryindex, setSelectedCategoryindex] = useState(0)
    const [activeCardIndex, setActiveCardIndex] = useState(0)
    //
    const scrollX = useRef(new Animated.Value(0)).current;

    // 分類bar Component
    const CategoryList = ({ navigation }) => {
        return (
            <View style={styles.CategoryListContainer}>
                {categories.map((item, index) => (
                    <TouchableOpacity key={index} activeOpacity={0.8} onPress={() => setSelectedCategoryindex(index)}>
                        <View>
                            {/* 第一次看到 點點點+style的用法  這邊的用途是 被點選的分類 顏色會不同*/}
                            <Text style={{ ...styles.CategoryListText, color: selectedCategoryindex == index ? COLORS.primary : COLORS.grey }}>{item}</Text>
                        </View>
                        {selectedCategoryindex == index && (
                            // 下底線
                            < View style={{ height: 3, width: 30, backgroundColor: COLORS.primary, marginTop: 2 }} />
                        )}
                    </TouchableOpacity>
                ))}
            </View>
        )
    }

    // 房間卡片 Component
    const Card = ({ hotel, index }) => {
        const inputRange = [
            (index - 1) * CardWidth,
            index * CardWidth,
            (index + 1) * CardWidth,
        ];
        // 房間卡片 透明度動畫
        const opacity = scrollX.interpolate({
            inputRange,
            outputRange: [0.7, 0, 0.7],
        });
        // 房間卡片大小 縮放動畫
        const scale = scrollX.interpolate({
            inputRange,
            outputRange: [0.8, 1, 0.8],
        });

        return (
            <TouchableOpacity
                activeOpacity={1}
                //控制 目前滑到的頁面 才能點擊 旁邊的卡片點了無作用 (需配合下面 寫的 CardNo判斷式)
                disabled={activeCardIndex != index}
                onPress={() => navigation.navigate("Details", hotel)}>
                <Animated.View style={{ ...styles.Card, transform: [{ scale }] }}>
                    {/* 卡片 半透明變色 遮罩 */}
                    <Animated.View style={{ ...styles.CardOverLay, opacity }} />
                    {/* 右上角 價錢標籤 */}
                    <View style={styles.PriceTag}>
                        <Text style={{ color: COLORS.white, fontSize: 20, fontWeight: "bold" }}>${hotel.price}</Text>
                    </View>
                    {/* 房間圖片 */}
                    <Image
                        source={hotel.image}
                        style={styles.CardImage}
                    />
                    {/* 房間簡介 */}
                    <View style={styles.CardDetails}>
                        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                            <View>
                                {/* 飯店名稱 */}
                                <Text style={{ fontSize: 17, fontWeight: "bold" }}>{hotel.name}</Text>
                                {/* 地址 */}
                                <Text style={{ fontSize: 12, color: COLORS.grey }}>{hotel.location}</Text>
                            </View>
                            <Icon name="bookmark-border" size={26} color={COLORS.primary} />
                        </View>
                        {/* 飯店評價星數 點閱數 */}
                        {/* 可改善的地方: 星數 跟瀏覽數 做成api 控制輸出 */}
                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            marginTop: 10,
                        }}>
                            <View style={{ flexDirection: 'row' }}>
                                <Icon name="star" size={15} color={COLORS.orange} />
                                <Icon name="star" size={15} color={COLORS.orange} />
                                <Icon name="star" size={15} color={COLORS.orange} />
                                <Icon name="star" size={15} color={COLORS.orange} />
                                <Icon name="star" size={15} color={COLORS.grey} />
                            </View>
                            <Text style={{ fontSize: 10, color: COLORS.grey }}>365reviews</Text>
                        </View>
                    </View>
                </Animated.View>
            </TouchableOpacity>
        )
    }
    // 房間卡片2 top hotel card
    const TopHotelCard = ({ hotel }) => {
        return (
            <View style={styles.TopHotelCard}>
                <View style={{
                    position: "absolute",
                    top: 5,
                    right: 5,
                    zIndex: 1,
                    flexDirection: "row"
                }}>
                    <Icon name="star"
                        size={15}
                        color={COLORS.orange} />
                    <Text style={{ color: COLORS.white, fontSize: 15, fontWeight: "bold" }}>5.0</Text>
                </View>
                <Image style={styles.TopHotelCardImage}
                    source={hotel.image} />
                <View style={{ paddingVertical: 5, paddingHorizontal: 10 }}>
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{hotel.name}</Text>
                    <Text style={{ fontSize: 7, color: COLORS.grey }}>{hotel.location}</Text>
                </View>
            </View>
        )
    }
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.white }}>
            {/* 標頭 */}
            <View style={styles.Header}>
                <View style={{ paddingBottom: 15 }}>
                    <Text style={{ fontSize: 30, fontWeight: "bold" }}>Find your hotel</Text>
                    <View style={{ flexDirection: "row" }}>
                        <Text style={{ fontSize: 30, fontWeight: "bold" }}>in</Text>
                        <Text style={{ fontSize: 30, fontWeight: "bold", color: COLORS.primary }}>Taipei</Text>
                    </View>
                </View>
                <Icon name="person-outline" size={38} color={COLORS.grey} />
            </View>
            {/* 標頭以下的區塊 都可滑動 */}
            <ScrollView showsVerticalScrollIndicator={false}>
                {/* 搜尋框 */}
                <View style={styles.SearchInputContainer}>
                    <Icon name="search" size={30} style={{ marginLeft: 20 }} />
                    <TextInput placeholder='Search' style={{ fontSize: 20, paddingLeft: 10 }} />
                </View>
                {/* 分類bar */}
                <CategoryList />
                {/* 房間展示的卡片 */}
                <View>
                    <Animated.FlatList
                        //判斷 目前 滑到第幾張 房間卡片
                        //影片中 只跳一次數字 我的 一次跳好幾筆(重覆的數值)
                        onMomentumScrollEnd={(e) => {
                            // console.log(Math.round(e.nativeEvent.contentOffset.x / CardWidth))
                            const CardNo = e.nativeEvent.contentOffset.x / CardWidth
                            setActiveCardIndex(CardNo)
                        }}
                        //
                        onScroll={Animated.event(
                            [{ nativeEvent: { contentOffset: { x: scrollX } } }],
                            { useNativeDriver: true },
                        )}
                        // FlatList 讓卡片橫排 用 horizontal 而不是 像一般 在外面加 flexDirection: row
                        horizontal
                        contentContainerStyle={{ paddingVertical: 30, paddingLeft: 20, paddingRight: CardWidth / 2 - 40 }}
                        data={hotels}
                        showsHorizontalScrollIndicator={false}
                        renderItem={({ item, index }) => <Card hotel={item} index={index} />}
                        // 定格效果 看不太出來
                        snapToInterval={CardWidth}
                    />
                </View>
                {/* 下方的區塊 */}
                <View style={{ flexDirection: "row", justifyContent: "space-between", marginHorizontal: 20 }}>
                    <Text style={{ fontWeight: "bold", color: COLORS.grey }}>Top Hotels</Text>
                    <Text style={{ color: COLORS.grey }}>Show all</Text>
                </View>
                <FlatList
                    data={hotels}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    contentContainerStyle={{ paddingLeft: 20, marginTop: 20, paddingBottom: 30 }}
                    // renderItem={({ item }) => <TopHotelCard hotel={item} />}
                    renderItem={({ item }) => <TopHotelCard hotel={item} />}
                />
            </ScrollView>
        </SafeAreaView>
    );
};


const styles = StyleSheet.create({
    Header: {
        flexDirection: "row",
        marginTop: 20,
        // 畫面兩個物件要左右頭尾放 用space-between
        justifyContent: "space-between",
        paddingHorizontal: 20,
    },
    SearchInputContainer: {
        height: 50,
        backgroundColor: COLORS.light,
        marginTop: 15,
        marginLeft: 20,
        borderBottomLeftRadius: 30,
        flexDirection: "row",
        alignItems: "center"
    },
    CategoryListContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginHorizontal: 20,
        marginTop: 30,
    },
    CategoryListText: {
        fontSize: 17,
        fontWeight: "bold",
    },
    Card: {
        width: CardWidth,
        height: 280,
        elevation: 15,
        marginRight: 20,
        borderRadius: 15,
        backgroundColor: COLORS.white,
    },
    PriceTag: {
        width: 80,
        height: 60,
        backgroundColor: COLORS.primary,
        position: "absolute",
        zIndex: 1,
        right: 0,
        borderTopRightRadius: 15,
        borderBottomLeftRadius: 15,
        justifyContent: "center",
        alignItems: "center",

    },
    CardImage: {
        width: "100%",
        height: 200,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
    },
    CardDetails: {
        width: "100%",
        height: 100,
        borderRadius: 15,
        backgroundColor: COLORS.light,
        position: "absolute",
        bottom: 0,
        padding: 20,
    },
    CardOverLay: {
        height: 280,
        backgroundColor: COLORS.white,
        position: 'absolute',
        zIndex: 100,
        width: CardWidth
    },
    TopHotelCard: {
        width: 120,
        height: 120,
        backgroundColor: COLORS.white,
        elevation: 15,
        marginHorizontal: 10,
        borderRadius: 10,
    },
    TopHotelCardImage: {
        width: "100%",
        height: 80,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    }
});

export default HomeScreen;

