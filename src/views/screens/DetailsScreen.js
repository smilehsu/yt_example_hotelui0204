import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, ImageBackground } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import COLORS from '../../consts/colors';



const DetailScreen = ({ navigation, route }) => {
    const item = route.params
    console.log(item)
    return (
        <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ backgroundColor: COLORS.white, paddingBottom: 20 }}>

            {/* 第一次用到 */}
            <ImageBackground
                style={styles.HeaderImage}
                source={item.image}
            >
                <View style={styles.Header}>
                    {/* 回上一頁 */}
                    <Icon name="arrow-back" size={28} color={COLORS.white} onPress={navigation.goBack} />
                    {/* 加入書籤 純展示 無作用 */}
                    <Icon name="bookmark-border" size={28} color={COLORS.white} />
                </View>
            </ImageBackground>
            <View>
                {/* 地址 icon */}
                <View style={styles.IconContainer}>
                    <Icon name="place" size={28} color={COLORS.white} />
                </View>
                <View style={{ marginTop: 20, paddingHorizontal: 20, }}>
                    {/* 飯店名稱 */}
                    <Text style={{ fontSize: 20, fontWeight: "bold" }}>{item.name}</Text>
                    <Text style={{
                        fontSize: 12,
                        fontWeight: "400",
                        color: COLORS.grey,
                        marginTop: 5,
                    }}>
                        {/* 飯店地址 */}
                        {item.location}</Text>
                    <View style={{ marginTop: 10, flexDirection: "row", justifyContent: "space-between" }}>
                        <View style={{ flexDirection: 'row' }}>
                            {/* 飯店評價 */}
                            <View style={{ flexDirection: "row" }}>
                                <Icon name="star" size={20} color={COLORS.orange} />
                                <Icon name="star" size={20} color={COLORS.orange} />
                                <Icon name="star" size={20} color={COLORS.orange} />
                                <Icon name="star" size={20} color={COLORS.orange} />
                                <Icon name="star" size={20} color={COLORS.grey} />
                            </View>
                            <Text style={{ fontSize: 18, fontWeight: "bold", marginLeft: 5 }}>4.0</Text>
                        </View>
                        {/* 飯店點閱數 */}
                        <Text style={{ fontSize: 13, color: COLORS.grey }}> 9487reviews</Text>
                    </View>
                    {/* 飯店簡介 */}
                    <View style={{ marginTop: 20 }}>
                        <Text style={{ lineHeight: 20, color: COLORS.grey }}>{item.details}</Text>
                    </View>
                </View>
                {/* 價錢 */}
                <View style={{ marginTop: 20, flexDirection: "row", justifyContent: "space-between", paddingLeft: 20, alignItems: "center" }}>
                    <Text style={{ fontSize: 20, fontWeight: "bold" }}>Price per night</Text>
                    <View style={styles.PriceTag}>
                        <Text style={{ fontSize: 16, fontWeight: "bold", color: COLORS.grey, marginLeft: 5 }}>${item.price}</Text>
                        <Text style={{ fontSize: 12, fontWeight: "bold", color: COLORS.grey, marginLeft: 5 }}>+早餐</Text>
                    </View>
                </View>
                {/* 訂房按鈕 */}
                <View style={styles.Btn}>
                    <Text style={{ fontSize: 24, fontWeight: "bold", color: COLORS.white }}>Book Now</Text>
                </View>
            </View>
        </ScrollView >
    );
};

const styles = StyleSheet.create({
    HeaderImage: {
        height: 400,
        borderBottomLeftRadius: 40,
        borderBottomRightRadius: 40,
        overflow: "hidden"
    },
    Header: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginTop: 40,
        marginHorizontal: 20,
    },
    IconContainer: {
        position: "absolute",
        width: 60,
        height: 60,
        backgroundColor: COLORS.primary,
        top: -30,
        right: 20,
        borderRadius: 30,
        justifyContent: "center",
        alignItems: "center",
    },
    PriceTag: {
        flex: 1,
        flexDirection: "row",
        height: 40,
        alignItems: "center",
        marginLeft: 40,
        paddingLeft: 20,
        backgroundColor: COLORS.secondary,
        borderTopLeftRadius: 20,
        borderBottomLeftRadius: 20,
    },
    Btn: {
        height: 55,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 40,
        backgroundColor: COLORS.primary,
        marginHorizontal: 20,
        borderRadius: 10
    },
});


export default DetailScreen;


