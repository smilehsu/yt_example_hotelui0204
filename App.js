import React from 'react';
//從上個範例的原始碼就有加 但我都沒加～ 看不出沒加會怎樣
import "react-native-gesture-handler";
import { View, Text } from 'react-native';
import { StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import COLORS from "./src/consts/colors";
import HomeScreen from "./src/views/screens/HomeScreen"
import DetailsScreen from "./src/views/screens/DetailsScreen"

const Stack = createNativeStackNavigator();


function App() {
  return (
    <NavigationContainer>
      <StatusBar barStyle="light-content" backgroundColor="rgba(0, 0, 0, 0)" translucent />
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Details" component={DetailsScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;